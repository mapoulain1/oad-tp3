#pragma once
#include "graph_t.h"
#include "solution_t.h"

#include <string>
#define HASH_SIZE 10000

using namespace std;

class solution
{
public:
	static int hash(solution_t& solution);
	static int cost(solution_t& solution);
	static int cost(solution_t& solution, graph_t &graph);
	static void save(tour_t TG,solution_t& solution, graph_t& graph, string filename);
	static solution_t cpSol(solution_t src);
	static int nbClients(solution_t& solution);
	static int missing(solution_t& solution, int nbClients);
};

