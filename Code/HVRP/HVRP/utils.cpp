#include "utils.h"

#include <stdlib.h>


int utils::random(int a, int b) {
	return (rand() % (b - a + 1)) + a;
}

float utils::random(void)
{
	return ((float)rand() / RAND_MAX);
}

void utils::swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void utils::swapReverse(int* a, int* b)
{
	int tmp;
	if (b > a) {
		while (b > a) {
			tmp = *a;
			*a = *b;
			*b = tmp;
			a++;
			b--;
		}
	}
	else {
		while (b < a) {
			tmp = *a;
			*a = *b;
			*b = tmp;
			a--;
			b++;
		}
	}

}

void utils::move(tour_t& tour, int from, int to)
{
	int tmp = tour[from];
	if (from > to) {
		for (int i = from; i > to; i--)
			tour[i] = tour[i - 1];
		tour[to] = tmp;
	}
	else {
		for (int i = from; i < to; i++)
			tour[i] = tour[i + 1];
		tour[to] = tmp;
	}
}

void utils::leftMove(tour_t& tour, int from, int to)
{
	for (int i = to; i < from; i++)
		tour[i] = tour[i+1];
}
