#pragma once

#include "graph_t.h"
#include "solution_t.h"

#include <string>

using namespace std;

class graph {
  public:
	static void load(string filename, graph_t *out);
	static void print(graph_t &graph);
	static int hash(solution_t &solution);
	static solution_t SPLIT(tour_t& tour, graph_t& graph);
	static void printSol(solution_t& sol);
	static tour_t InvSPLIT(solution_t& sol, graph_t& graph);
};