#pragma once

#define MAX_DISTANCE_MATRIX 251

typedef struct {
	int size;
	int distances[MAX_DISTANCE_MATRIX][MAX_DISTANCE_MATRIX];
	int demand [MAX_DISTANCE_MATRIX];
	int truckCapacity;
}graph_t;

