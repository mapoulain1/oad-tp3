#include "test.h"
#include "utils.h"
#include <iostream>

#include "graph.h"
#include "recherche.h"
#include "solution.h"
#include "tour.h"
#include <vector>
#include <chrono>


using namespace std;

void usage(void) {
	cout << "usage :" << endl;
	cout << "\tvrp.exe [input file]" << endl;
}

void handleCommandLine(int argc, char* argv[]) {
	if (argc != 2) {
		usage();
	}
	else {

		string input = string(argv[1]);
		string output;

		size_t lastindex = input.find_last_of(".");
		if (lastindex != string::npos)
			output = input.substr(0, lastindex) + "_solution.txt";
		else
			output = input + "_solution.txt";

		graph_t graph{};
		graph::load(input, &graph);
		tour_t TG = Tour::generateRandom(graph, 5);
		solution_t sol;
		solution_t tmp = graph::SPLIT(TG, graph);
		int before = solution::cost(tmp, graph);
		
		auto t_start = std::chrono::high_resolution_clock::now();
		recherche::GRASP(TG, sol, graph, 100, 6);
		auto t_end = std::chrono::high_resolution_clock::now();
		
		int after = solution::cost(sol, graph);
		double elapsed_time_ms = std::chrono::duration<double, std::milli>(t_end - t_start).count();
		
		
		cout << "Gained : " << before << " - " << after << " = " << before - after << " -> " << (before - after) / (float)before * 100 << "%" << endl;
		cout << "Processing done in " << elapsed_time_ms << " ms" << endl;
		
		solution::save(TG, sol, graph, output);
		cout << "Saved to " << output << endl;
	}
	(void)getc(stdin);
}

int main(int argc, char* argv[]) {
	srand(258197);
	handleCommandLine(argc, argv);
	return 0;
}
