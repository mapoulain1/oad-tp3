#pragma once

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))


#include "tour_t.h"

class utils {
public:
	/// <summary>
	/// G�n�re un random
	/// </summary>
	/// <param name="a">Min</param>
	/// <param name="b">Max</param>
	/// <returns></returns>
	static int random(int a, int b);

	/// <summary>
	/// G�n�re un random entre 0 et 1
	/// </summary>
	/// <returns></returns>
	static float random(void);

	/// <summary>
	/// �change deux valeurs
	/// </summary>
	/// <param name="a">A</param>
	/// <param name="b">B</param>
	static void swap(int* a, int* b);


	/// <summary>
	/// �change deux valeurs et inverse toutes les autres entre les deux
	/// </summary>
	/// <param name="a">A</param>
	/// <param name="b">B</param>
	static void swapReverse(int* a, int* b);


	
	/// <summary>
	/// D�cale une valeur dans un tour
	/// </summary>
	/// <param name="tour">Tour</param>
	/// <param name="from">Index depuis</param>
	/// <param name="to">Index vers</param>
	static void move(tour_t &tour, int from, int to);



	/// <summary>
	/// D�calage � gauche
	/// </summary>
	/// <param name="tour">Tour</param>
	/// <param name="from">Index depuis</param>
	/// <param name="to">Index vers</param>
	static void leftMove(tour_t &tour, int from, int to);

};

