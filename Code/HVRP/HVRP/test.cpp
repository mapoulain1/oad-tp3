#include "test.h"

#include "graph.h"
#include "recherche.h"
#include "solution.h"
#include "tour.h"
#include <iostream>
#include <vector>

using namespace std;

void Test::testLoadFile(void) {

	graph_t graph{};
	graph::load("VRP63.txt", &graph);
	graph::print(graph);
}

void Test::testTourGeant(void) {
	graph_t graph{};
	graph::load("VRP63.txt", &graph);
	for (int i = 0; i < 10; i++) {
		tour_t tour = Tour::generateRandom(graph, 5);
		Tour::print(tour);
	}
}

void Test::testInnerOp(void) {
	graph_t graph{};
	graph::load("VRP63.txt", &graph);
	//tour_t tour = Tour::generateRandom(graph);
	tour_t tour = Tour::generate(graph);
	solution_t solution = graph::SPLIT(tour, graph);

	for (int i = 0; i < solution.nbTours; i++) {
		recherche::innerSwapOp(solution, graph, i);
	}
}

void Test::testInsertOp(void) {
	graph_t graph{};
	graph::load("VRP63.txt", &graph);
	//tour_t tour = Tour::generateRandom(graph);
	tour_t tour = Tour::generate(graph);
	solution_t solution = graph::SPLIT(tour, graph);

	for (int i = 0; i < solution.nbTours; i++) {
		recherche::innerInsertOp(solution, graph, i);
	}
}

void Test::testInterOp(void) {
	graph_t graph{};
	graph::load("VRP63.txt", &graph);
	//tour_t tour = Tour::generateRandom(graph);
	tour_t tour = Tour::generate(graph);
	solution_t solution = graph::SPLIT(tour, graph);

	for (int i = 0; i < solution.nbTours - 1; i++) {
		for (int j = i + 1; j < solution.nbTours; j++) {
			recherche::interSwapOp(solution, graph, i, j);
		}
	}
}

void Test::testSplit(void) {
	graph_t graph{};
	graph::load("VRP63.txt", &graph);
	tour_t tour = Tour::generateRandom(graph, 5);
	Tour::print(tour);
	solution_t sol = graph::SPLIT(tour, graph);
	graph::printSol(sol);
}

void Test::testInvSplit(void) {
	graph_t graph{};
	graph::load("VRP63.txt", &graph);
	tour_t tour = Tour::generateRandom(graph, 5);
	Tour::print(tour);
	solution_t sol = graph::SPLIT(tour, graph);
	graph::printSol(sol);
	tour = graph::InvSPLIT(sol, graph);
	Tour::print(tour);

}

void Test::testGRASP(void) {
	srand(25);
	graph_t graph{};
	graph::load("VRP63.txt", &graph);
	tour_t tour = Tour::generateRandom(graph, 5);
	//tour_t tour = Tour::generate(graph);
	solution_t sol = graph::SPLIT(tour, graph);
	int before = solution::cost(sol);
	recherche::GRASP(tour,sol, graph);
	int after = solution::cost(sol);
	cout << "Gained : " << before - after << " -> " << (before - after) / (float)before * 100 << "%" << endl;
	tour_t result = graph::InvSPLIT(sol, graph);
}
