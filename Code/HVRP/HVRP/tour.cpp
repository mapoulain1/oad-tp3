#include "tour.h"

#include <vector>
#include <iostream>

#include "utils.h"

using namespace std;
tour_t Tour::generate(graph_t& graph)
{
	tour_t tour(graph.size + 2);
	vector<int> tab(graph.size);

	int length = graph.size;
	int random;
	int counter = 0;

	for (int i = 0; i < graph.size; i++)
		tab[i] = i + 1;

	tour[counter] = 0;
	counter++;
	for (; length > 0; length--) {
		random = utils::random(0, length - 1);
		tour[counter] = tab[random];
		tab[random] = tab[length - 1];
		counter++;
	}
	tour[counter] = 0;
	isValid(tour);
	return tour;
}


tour_t Tour::generateRandom(graph_t& graph, int neighbor)
{
	tour_t tour(graph.size + 1);
	vector<int> tab(graph.size);
	vector<int> neighborsDistances(neighbor + 1);
	vector<int> neighborsID(neighbor + 1);

	int length = graph.size;
	int random = 0;
	int counter = 0;
	int neighborCounter;
	int neighborFound;

	for (int i = 0; i < graph.size; i++)
		tab[i] = i;
	tour[counter] = 0;
	counter++;
	for (; length > 0; length--) {
		for (int i = 0; i < neighbor + 1; i++) {
			neighborsDistances[i] = INT32_MAX;
			neighborsID[i] = -1;
		}
		for (int i = 1; i < graph.size; i++) {
			if (tab[i] == -1 || graph.distances[random][tab[i]] == 0)
				continue;
			neighborCounter = neighbor;
			neighborsDistances[neighborCounter] = graph.distances[random][tab[i]];
			neighborsID[neighborCounter] = tab[i];
			while (neighborCounter > 0 && neighborsDistances[neighborCounter] < neighborsDistances[neighborCounter - 1]) {
				utils::swap(&neighborsID[neighborCounter], &neighborsID[neighborCounter - 1]);
				utils::swap(&neighborsDistances[neighborCounter], &neighborsDistances[neighborCounter - 1]);
				neighborCounter--;
			}
		}
		neighborFound = 0;
		for (int i = 0; i < neighbor; i++)
			if (neighborsID[i] != -1)
				neighborFound++;
		if (neighborFound == 0) 
			break;
		random = utils::random(0, MIN(neighbor - 1, neighborFound - 1));
		random = neighborsID[random];
		tour[counter] = tab[random];
		tab[random] = -1;
		counter++;
	}
	return tour;
}

void Tour::print(tour_t& tour)
{
	int len = tour.size();
	for (int i = 0; i < len; i++)
	{
		cout << tour[i] << ' ';
	}
	cout << endl;
}

bool Tour::isValid(tour_t& tour, bool print)
{
	int len = tour.size();
	int numberOfInvalid = 0;
	vector<short> checker = vector<short>(len+1);
	for (int i = 0; i < len; i++)
		checker[tour[i]]++;

	for (int i = 0; i < len+1; i++) {
		if (checker[i] != 1) {
			cout << "Error on : " << i << " - count : " << checker[i] << endl;
			numberOfInvalid++;
		}
	}
	return numberOfInvalid == 0;
}

bool Tour::isBuggy(tour_t& tour)
{
	int size = tour.size();
	for (int i = 1; i < size - 1 ; i++)
	{
		if (tour[i] == 0) {
			return true;
		}
	}
	return false;
}

tour_t Tour::cpTour(tour_t src)
{
	tour_t ret;
	for (int i = 0; i < src.size(); i++)
	{
		ret.push_back(src[i]);
	}
	return ret;
}

int Tour::nbClients(tour_t& tour)
{
	int counter = 0;
	for (int i = 0; i < tour.size(); i++)
	{
		if (tour[i] != 0) {
			counter++;
		}
	}
	return counter;
}

void Tour::clean(tour_t& tour)
{
	for (int i = 1; i < tour.size()-1; i++)
	{
		if (tour[i] == 0) {
			tour.erase(tour.begin() + i);
			i--;
		}
	}
}

