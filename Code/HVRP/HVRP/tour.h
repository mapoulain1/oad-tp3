#pragma once
#include "tour_t.h"
#include "graph_t.h"

#include <vector>

using namespace std;

class Tour {
public:
	static tour_t generate(graph_t& graph);
	static tour_t generateRandom(graph_t& graph, int neighbor = 5);
	static void print(tour_t &tour);
	static bool isValid(tour_t& tour, bool print = false);
	static bool isBuggy(tour_t& tour);
	static tour_t cpTour(tour_t src);
	static int nbClients(tour_t& tour);
	static void clean(tour_t& tour);

};

