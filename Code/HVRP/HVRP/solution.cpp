#include "solution.h"

#include "tour.h"
#include "graph.h"

#include <iostream>
#include <fstream>
using namespace std;


int solution::hash(solution_t& solution)
{
	int length = solution.nbTours;
	int sum = 0;
	int innerLength;
	for (int i = 0; i < length; i++)
	{
		innerLength = solution.toursLength[i];
		sum += solution.tours[i][1] * solution.tours[i][1];
		sum += solution.tours[i][innerLength - 2] * solution.tours[i][innerLength - 2];
	}
	sum %= HASH_SIZE;
	return sum;
}

int solution::cost(solution_t& solution)
{
	int sum = 0;
	for (int i = 0; i < solution.nbTours; i++)
		sum += solution.coutTours[i];
	return sum;
}

int solution::cost(solution_t& solution, graph_t& graph)
{
	int sum = 0;
	for (int i = 0; i < solution.nbTours; i++) {
		if (solution.toursLength[i] > 2) {
			for (int j = 0; j < solution.toursLength[i] - 1; j++) {
				sum += graph.distances[solution.tours[i][j]][solution.tours[i][j + 1]];
			}
		}
	}
	return sum;
}

void solution::save(tour_t TG, solution_t& solution, graph_t& graph, string filename)
{
	ofstream myfile;
	myfile.open(filename);
	int sum = 0;
	for (int i = 0; i < solution.nbTours; i++) if (solution.toursLength[i] > 2) sum++;

	myfile << sum << endl;

	for (int i = 0; i < solution.nbTours; i++) {
		if (solution.toursLength[i] > 2) {
			myfile << solution.toursLength[i] << " :\t";
			for (int j = 0; j < solution.toursLength[i]; j++)
				myfile << solution.tours[i][j] << '\t';
			myfile << endl;
		}
	}
	myfile << endl;
	myfile << solution::cost(solution, graph) << endl;
	myfile.close();
}

solution_t solution::cpSol(solution_t src)
{
	solution_t ret;
	int taille = src.TG.size();

	ret.tours = vector<tour_t>(taille);
	ret.nbTours = src.nbTours;
	for (int i = 0; i < taille; i++)	ret.TG.push_back(src.TG[i]);

	for (int i = 0; i < ret.nbTours; i++)
	{

		ret.poidTours.push_back(src.poidTours[i]);
		ret.coutTours.push_back(src.coutTours[i]);
		ret.toursLength.push_back(src.toursLength[i]);

		for (int j = 0; j < ret.toursLength[i]; j++)	ret.tours[i].push_back(src.tours[i][j]);
	}
	return ret;
}

int solution::nbClients(solution_t& solution)
{
	int counter = 0;
	for (int i = 0; i < solution.nbTours; i++)
	{
		if (solution.toursLength[i] > 2) {
			counter += Tour::nbClients(solution.tours[i]);
		}
	}
	return counter;
}

int solution::missing(solution_t& solution, int nbClients)
{
	vector<int> clients = vector<int>(nbClients);
	int counter = 0;
	for (int i = 0; i < solution.nbTours; i++)
		for (int j = 0; j < solution.toursLength[i]; j++)
			clients[solution.tours[i][j]]++;

	cout << "Missing : ";
	for (int i = 0; i < nbClients; i++)
		if (clients[i] == 0) {
			cout << i << "\t";
			counter++;
		}

	cout << endl;
	return counter != 0;
}

