#include "graph.h"
#include <iostream>

#include <fstream>
#include <iostream>
#include <string>

void graph::load(string filename, graph_t *out) {
	ifstream flot(filename.c_str());
	int dummy;
	flot >> out->size;
	(out->size)++;
	flot >> out->truckCapacity;
	flot >> dummy;

	for (int i = 0; i < out->size; i++) {
		for (int j = 0; j < out->size; j++) {
			flot >> out->distances[i][j];
		}
	}
	for (int i = 1; i < out->size; i++) {
		flot >> out->demand[i];
	}

	flot.close();
}

void graph::print(graph_t &graph) {
	int i;
	std::cout << "Capacite des camions\t" << graph.truckCapacity << "\n";
	for (i = 0; i < graph.size; i++) {
		std::cout << "Ville " << i << "\tdemande " << graph.demand[i] << "\n";
		for (int j = 0; j < graph.size; j++) {
			std::cout << graph.distances[i][j] << "\t";
		}
		std::cout << "\n";
	}

	std::cout << "\n";
}

void graph::printSol(solution_t &sol) {
	for (int i = 0; i < sol.nbTours; i++) {
		for (int j = 0; j < sol.toursLength[i]; j++) {
			std::cout << sol.tours[i][j] << " \t";
		}
		std::cout << "\n"
				  << sol.poidTours[i] << " \t" << sol.coutTours[i] << " \t" << sol.toursLength[i] << " \n";
	}
}

solution_t graph::SPLIT(tour_t& tour, graph_t &graph) {
	int cout = 0;
	int capa = 0;
	int j;
	int indiceI, indiceJ;
	int taille = tour.size();
	solution_t sol;
	sol.TG = tour;
	sol.nbTours = 0;
	sol.tours = vector<tour_t>(taille);
	int tourActuel;
	vector<int> pere = vector<int>(taille);
	vector<int> coutTmp = vector<int>(taille);
	fill(coutTmp.begin(), coutTmp.end(), INT_MAX);
	coutTmp[0] = 0;
	
	for (int indiceI = 0; indiceI < tour.size() - 2; indiceI++) {
		int i = tour[indiceI];
		indiceJ = indiceI + 1;
		j = tour[indiceJ];

		do {

			if (indiceJ == indiceI + 1) {
				capa = graph.demand[j];
				cout = graph.distances[0][j] + graph.distances[j][0];
				if (coutTmp[i] + cout < coutTmp[j]) {
					coutTmp[j] = coutTmp[i] + cout;
					pere[indiceJ] = indiceI;
				}
			} else {
				cout += graph.distances[j - 1][j] + graph.distances[j][0] - graph.distances[j - 1][0];
				capa += graph.demand[j];
				if (coutTmp[i] + cout < coutTmp[j]) {
					coutTmp[j] = coutTmp[i] + cout;
					pere[indiceJ] = indiceI;
				}
			}

			indiceJ++;
			j = tour[indiceJ];
		} while (indiceJ < taille - 2 && capa + graph.demand[j] <= graph.truckCapacity);
	}
	//pushback
	indiceJ--;
	j = tour[indiceJ];
	int prec;
	int compte;
	while (j) {
		prec = pere[indiceJ];
		while (indiceJ > 0 && pere[indiceJ] == prec)
			indiceJ--;
		int minIndice = indiceJ;
		indiceJ++;
		j = tour[indiceJ];
		sol.tours[sol.nbTours].push_back(sol.TG[0]);
		sol.tours[sol.nbTours].push_back(j);
		cout = graph.distances[0][j] + graph.distances[j][0];
		capa = graph.demand[j];
		compte = 2;
		indiceJ++;

		while (pere[indiceJ] == prec) {
			j = tour[indiceJ];
			sol.tours[sol.nbTours].push_back(j);
			cout += graph.distances[tour[indiceJ - 1]][j] + graph.distances[tour[indiceJ - 1]][0] - graph.distances[j][0];
			capa += graph.demand[j];
			compte++;
			indiceJ++;
		}
		compte++;
		sol.tours[sol.nbTours].push_back(sol.TG[0]);
		sol.toursLength.push_back(compte);
		sol.coutTours.push_back(cout);
		sol.poidTours.push_back(capa);
		sol.nbTours++;
		indiceJ = minIndice;
		j = tour[indiceJ];
	}
	return sol;
}

tour_t graph::InvSPLIT(solution_t& sol, graph_t& graph) {
	tour_t tour;
	tour.push_back(0);
	int reste[1000];
	int tourChoisi = 0,indiceR = 0;
	for (int i = 0; i < sol.nbTours; i++)	reste[i] = i;
	for (int i = sol.nbTours - 1; i >= 0; i--) {
		indiceR = rand() % (i+1);
		tourChoisi = reste[indiceR];
		
		reste[indiceR] = reste[i];
		for (int j = 1; j < sol.toursLength[tourChoisi] - 1; j++) {
			tour.push_back(sol.tours[tourChoisi][j]);
		}
	}
	tour.push_back(0);
	return tour;
}
