#include "recherche.h"
#include "utils.h"
#include "solution.h"
#include "graph.h"
#include "tour.h"

#include <iostream>

using namespace std;

int recherche::innerSwapOp(solution_t& solution, graph_t& graph, int indexTour)
{
	if (indexTour >= solution.nbTours || solution.toursLength[indexTour] < 4)
		return 0;

	tour_t& tour = solution.tours[indexTour];

	int tourLen = solution.toursLength[indexTour];
	int delta1;
	int delta2;
	int beta1;
	int beta2;
	int totalDelta;
	int changes = 0;

	for (int i = 1; i < tourLen - 2; i++) {
		delta1 = graph.distances[tour[i]][tour[i + 1]];
		for (int j = i + 1; j < tourLen - 1; j++) {
			delta2 = graph.distances[tour[j]][tour[j + 1]];
			beta1 = graph.distances[tour[i]][tour[j]];
			beta2 = graph.distances[tour[i + 1]][tour[j + 1]];
			totalDelta = beta1 + beta2 - delta1 - delta2;
			if (totalDelta < 0) {
				utils::swapReverse(&tour[i], &tour[j]);
				solution.coutTours[indexTour] = MAX(solution.coutTours[indexTour] + totalDelta, 0);
				changes++;
			}
		}
	}
	return changes;
}

int recherche::innerInsertOp(solution_t& solution, graph_t& graph, int indexTour)
{
	if (indexTour >= solution.nbTours || solution.toursLength[indexTour] < 4)
		return 0;

	tour_t& tour = solution.tours[indexTour];


	int tourLen = solution.toursLength[indexTour];
	int delta1;
	int delta2;
	int totalDelta;
	int changes = 0;

	for (int i = 1; i < tourLen - 2; i++)
	{
		delta1 = graph.distances[tour[i - 1]][tour[i + 1]] - graph.distances[tour[i - 1]][tour[i]] - graph.distances[tour[i]][tour[i + 1]];
		for (int j = i + 1; j < tourLen - 1; j++)
		{
			delta2 = graph.distances[tour[j - 1]][tour[j]] + graph.distances[tour[j]][tour[j + 1]] - graph.distances[tour[j - 1]][tour[j + 1]];
			totalDelta = delta1 + delta2;
			if (totalDelta < 0) {
				utils::move(tour, j, i);
				solution.coutTours[indexTour] = MAX(solution.coutTours[indexTour] + totalDelta, 0);
				changes++;
			}
		}
	}
	return changes;
}

int recherche::interSwapOp(solution_t& solution, graph_t& graph, int indexTour1, int indexTour2)
{
	if (indexTour1 >= solution.nbTours || indexTour2 >= solution.nbTours || solution.toursLength[indexTour1] < 4 || solution.toursLength[indexTour2] < 4)
		return 0;

	tour_t& tour1 = solution.tours[indexTour1];
	tour_t& tour2 = solution.tours[indexTour2];
	tour_t tmp1 = vector<int>(graph.size);
	tour_t tmp2 = vector<int>(graph.size);

	int tour1Len = solution.toursLength[indexTour1];
	int tour2Len = solution.toursLength[indexTour2];
	int minus1, minus2, minus3, minus4;
	int plus1, plus2, plus3, plus4;
	int totalDelta;
	int weight1, weight2, totalWeight1, totalWeight2;
	int changes = 0;

	for (int i = 1; i < tour1Len - 2; i++)
	{
		minus1 = graph.distances[tour1[i]][tour1[i + 1]];
		for (int j = i + 1; j < tour1Len - 2; j++)
		{
			weight1 = 0;
			minus2 = graph.distances[tour1[j]][tour1[j + 1]];
			for (int e = i + 1; e <= j; e++) {
				weight1 += graph.demand[tour1[e]];
				tmp1[e - i - 1] = tour1[e];
			}


			for (int k = 1; k < tour2Len - 2; k++)
			{
				minus3 = graph.distances[tour2[k]][tour2[k + 1]];
				for (int l = k + 1; l < tour2Len - 2; l++)
				{
					weight2 = 0;
					minus4 = graph.distances[tour2[l]][tour2[l + 1]];
					for (int e = k + 1; e <= l; e++) {
						tmp2[e - k - 1] = tour2[e];
						weight2 += graph.demand[tour2[e]];
					}

					plus1 = graph.distances[tour1[i]][tour2[l]];
					plus2 = graph.distances[tour1[j]][tour2[k]];
					plus3 = graph.distances[tour1[i + 1]][tour2[l + 1]];
					plus4 = graph.distances[tour1[j + 1]][tour2[k + 1]];
					totalDelta = plus1 + plus2 + plus3 + plus4 - minus1 - minus2 - minus3 - minus4;

					if (totalDelta < 0) {
						totalWeight1 = MAX(solution.poidTours[indexTour1] + weight2 - weight1, 0);
						totalWeight2 = MAX(solution.poidTours[indexTour2] + weight1 - weight2, 0);
						if (totalWeight1 < graph.truckCapacity && totalWeight2 < graph.truckCapacity) {
							// en commentaire car bug pour l'instant que (j - i) != de (l - k)

							/*for (int counter = 0, e = i + 1; e <= j; e++, counter++) tour1[e] = tmp2[counter];
							for (int counter = 0, e = k + 1; e <= l; e++, counter++) tour2[e] = tmp1[counter];
							Tour::clean(tour1);
							Tour::clean(tour2);

							utils::swapReverse(&tour1[i + 1], &tour1[j]);
							utils::swapReverse(&tour2[k + 1], &tour2[l]);

							tour1Len = solution.toursLength[indexTour1] = tour1.size();
							tour2Len = solution.toursLength[indexTour2] = tour2.size();

							solution.coutTours[indexTour1] = MAX(solution.coutTours[indexTour1] + plus1 + plus4 - minus1 - minus2, 0);
							solution.coutTours[indexTour2] = MAX(solution.coutTours[indexTour2] + plus2 + plus3 - minus3 - minus4, 0);

							solution.poidTours[indexTour1] = totalWeight1;
							solution.poidTours[indexTour2] = totalWeight2;
							*/
							changes++;
						}
					}
				}
			}
		}
	}
	return changes;
}

int recherche::interInsertOp(solution_t& solution, graph_t& graph, int indexTour1, int indexTour2)
{
	if (indexTour1 >= solution.nbTours || indexTour2 >= solution.nbTours || solution.toursLength[indexTour1] < 3 || solution.toursLength[indexTour2] < 3)
		return 0;

	tour_t& tour1 = solution.tours[indexTour1];
	tour_t& tour2 = solution.tours[indexTour2];

	int tour1Len = solution.toursLength[indexTour1];
	int tour2Len = solution.toursLength[indexTour2];
	int minus1, minus2, minus3;
	int plus1, plus2, plus3;
	int totalDelta;
	int changes = 0;


	for (int i = 1; i < tour1Len - 1; i++)
	{
		if (tour1Len < 3)
			continue;

		minus1 = graph.distances[tour1[i - 1]][tour1[i]];
		minus2 = graph.distances[tour1[i]][tour1[i + 1]];
		plus1 = graph.distances[tour1[i - 1]][tour1[i + 1]];
		for (int j = 0; j < tour2Len - 2; j++)
		{
			if (tour2Len < 3 || graph.demand[tour1[i]] + solution.poidTours[indexTour2] > graph.truckCapacity) {
				continue;
			}
			minus3 = graph.distances[tour2[j]][tour2[j + 1]];
			plus2 = graph.distances[tour2[j]][tour1[i]];
			plus3 = graph.distances[tour1[i]][tour2[j + 1]];
			totalDelta = plus1 + plus2 + plus3 - minus1 - minus2 - minus3;
			if (totalDelta < 0) {
				solution.coutTours[indexTour2] += totalDelta;
				solution.poidTours[indexTour1] -= graph.demand[tour1[i]];
				solution.poidTours[indexTour2] += graph.demand[tour1[i]];
				tour2.push_back(tour1[i]);
				utils::move(tour2, tour2Len, j + 2);
				tour1.erase(tour1.begin() + i);

				solution.toursLength[indexTour1]--;
				tour1Len = solution.toursLength[indexTour1];
				solution.toursLength[indexTour2]++;
				tour2Len = solution.toursLength[indexTour2];

				changes++;
				return changes;
			}

		}
	}

	return changes;
}

void recherche::GRASP(tour_t& TG, solution_t& solution, graph_t& graph, int maxIteration, int noChangeMaxIteration)
{
	int hash[HASH_SIZE];
	solution_t currSol, solBest;
	tour_t tmpTG, bestTG;
	int bestCout, currCout, coutBestVoisin, tmp, i1, i2, iterationVoisins;
	bestCout = recherche::RechercheLocale(TG, solution, graph, 100, 100);
	int currHash = solution::hash(solution);
	hash[currHash] = 1;
	int noChanges = 0;

	for (int i = 0; i < maxIteration; i++)
	{
		cout << (float)i * 100 / maxIteration << "%" << endl;
 		coutBestVoisin = INT32_MAX;
		for (int j = 0; j < 10; j++) // nb voisins
		{
			iterationVoisins = 0;
			do
			{
				iterationVoisins++;
				currSol = solution::cpSol(solution);
				tmpTG = Tour::cpTour(TG);
				i1 = 1 + (rand() % (tmpTG.size() - 2));
				do
				{
					i2 = 1 + (rand() % (tmpTG.size() - 2));
				} while (i1 == i2);
				
				tmp = tmpTG[i1];
				tmpTG[i1] = tmpTG[i2];
				tmpTG[i2] = tmp;

				currCout = recherche::RechercheLocale(tmpTG, currSol, graph, 20, 5);
				tmp = solution::hash(solution);
			} while (!hash[tmp] == 1 && iterationVoisins < 100);
			hash[tmp] = 1;

			if (currCout < coutBestVoisin)
			{
				coutBestVoisin = currCout;
				solBest = solution::cpSol(currSol);
				bestTG = Tour::cpTour(tmpTG);
			}
		}
		if (coutBestVoisin < bestCout)
		{
			TG = Tour::cpTour(bestTG);
			bestCout = coutBestVoisin;
			solution = solution::cpSol(solBest);
			noChanges = 0;
		}
		noChanges++;
		if (noChanges > noChangeMaxIteration) {
			cout << "No changes in the best solution for the past " << noChanges << " iterations, now exiting GRASP" << endl << endl;
			break;
		}
	}

}

int recherche::RechercheLocale(tour_t& TG, solution_t& solution, graph_t& graph, int maxIteration, int noChangeMaxIteration)
{
	vector<float> probabilities = vector<float>(4, 0.5f);
	vector<int> changes = vector<int>(4);
	solution = graph::SPLIT(TG, graph);
	int lastBest = solution::cost(solution);
	int current = 0;
	int noChange = 0;
	
	for (int i = 0; i < maxIteration; i++)
	{
		fill(changes.begin(), changes.end(), 0);
		current = solution::cost(solution);
		if (current < lastBest) {
			lastBest = current;
			noChange = 0;
		}

		if (utils::random() < probabilities[0]) {
			for (int j = 0; j < solution.nbTours; j++)
				changes[0] += innerSwapOp(solution, graph, j);
			if (changes[0] == 0) probabilities[0] = MAX(probabilities[0] - PROBABILITY_DELTA, PROBABILITY_LOWER_BOUND);
			if (changes[0] > PROBABILITY_CHANGES_TRIGGER) probabilities[0] = MIN(probabilities[0] + PROBABILITY_DELTA, PROBABILITY_UPPER_BOUND);
		}

		if (utils::random() < probabilities[1]) {
			for (int j = 0; j < solution.nbTours; j++)
				changes[1] += innerInsertOp(solution, graph, j);
			if (changes[1] == 0) probabilities[1] = MAX(probabilities[1] - PROBABILITY_DELTA, PROBABILITY_LOWER_BOUND);
			if (changes[1] > PROBABILITY_CHANGES_TRIGGER) probabilities[1] = MIN(probabilities[1] + PROBABILITY_DELTA, PROBABILITY_UPPER_BOUND);
		}
		
		if (utils::random() < probabilities[2]) {
			for (int j = 0; j < solution.nbTours; j++)
				for (int k = j + 1; k < solution.nbTours; k++)
					changes[2] += interSwapOp(solution, graph, j, k);
			if (changes[2] == 0) probabilities[2] = MAX(probabilities[2] - PROBABILITY_DELTA, PROBABILITY_LOWER_BOUND);
			if (changes[2] > PROBABILITY_CHANGES_TRIGGER) probabilities[2] = MIN(probabilities[2] + PROBABILITY_DELTA, PROBABILITY_UPPER_BOUND);
		}
		
		
		if (utils::random() < probabilities[3]) {
			for (int j = 0; j < solution.nbTours; j++)
				for (int k = j + 1; k < solution.nbTours; k++)
					changes[3] += interInsertOp(solution, graph, j, k);
			if (changes[3] == 0) probabilities[3] = MAX(probabilities[3] - PROBABILITY_DELTA, PROBABILITY_LOWER_BOUND);
			if (changes[3] > PROBABILITY_CHANGES_TRIGGER) probabilities[3] = MIN(probabilities[3] + PROBABILITY_DELTA, PROBABILITY_UPPER_BOUND);
		}
		

		noChange++;
		if (noChange == noChangeMaxIteration)
			break;
	}
	TG = graph::InvSPLIT(solution, graph);
	return lastBest;
}

