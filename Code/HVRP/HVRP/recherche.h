#pragma once

#include "solution_t.h"
#include "graph_t.h"
#include"tour_t.h"

#define MAX_ITERATION_INNER_2OP 200
#define PROBABILITY_LOWER_BOUND 0.2
#define PROBABILITY_UPPER_BOUND 0.8
#define PROBABILITY_DELTA 0.05
#define PROBABILITY_CHANGES_TRIGGER 5

class recherche
{
public:
	static int innerSwapOp(solution_t& solution, graph_t& graph, int indexTour);
	static int innerInsertOp(solution_t& solution, graph_t& graph, int indexTour);
	static int interSwapOp(solution_t& solution, graph_t& graph, int indexTour1, int indexTour2);
	static int interInsertOp(solution_t& solution, graph_t& graph, int indexTour1, int indexTour2);
	static void GRASP(tour_t& TG,solution_t& solution, graph_t& graph, int maxIteration = 1000, int noChangeMaxIteration = 10000);
	static int RechercheLocale(tour_t& TG,solution_t& solution, graph_t& graph, int maxIteration = 20, int noChangeMaxIteration = 4);

};

