#pragma once
#include "tour_t.h"
#include <vector>

using namespace std;

typedef struct {
	int nbTours;
	tour_t TG;
	vector<int> poidTours;
	vector<int> coutTours;
	vector<tour_t> tours;
	vector<int> toursLength;
}solution_t;

