#pragma once
class Test {
  public:
	static void testLoadFile(void);
	static void testTourGeant(void);
	static void testInnerOp(void);
	static void testInsertOp(void);
	static void testInterOp(void);
	static void testSplit(void);
	static void testInvSplit(void);
	static void testGRASP(void);
};
